#ifndef ENCORE_FLOW_H
#define ENCORE_FLOW_H

#include "pony.h"

typedef struct future flow_t;
extern pony_type_t flow_type;

/**
 * Create a new flow.
 */
flow_t* flow_mk(pony_ctx_t** ctx, pony_type_t* type);

/**
 * Create a new flow, fulfiled with value @a value.
 * 
 * This is used when casting values from T to Flow[T]. For example, consider a 
 * function returning Flow[int] ; if a final statement in the function returns a
 * value of type int, we need to transform this value into a Flow. This is where
 * this function comes in. 
 */
flow_t* flow_mk_from_value(pony_ctx_t** cttx, pony_type_t* type, 
                           encore_arg_t value);

encore_arg_t flow_get(pony_ctx_t** cttx, flow_t* fut);
void flow_fulfil(pony_ctx_t** cttx, flow_t* flow, encore_arg_t value);
void flow_fulfil_set_type(pony_ctx_t** cttx, flow_t* flow, encore_arg_t value,
                          pony_type_t* type);
bool flow_fulfilled(flow_t* const flow);

flow_t *flow_chain_actor(pony_ctx_t **ctx, flow_t *fut, pony_type_t *type,
                         closure_t *c);
void flow_chain_with_flow(pony_ctx_t **ctx, flow_t *fut, pony_type_t *type,
                         closure_t *c, future_t *r);

void flow_await(pony_ctx_t** cttx, flow_t* fut);

void flow_trace(pony_ctx_t* ctx, void* p);

#endif
